
'''
created on 31.05.2015

@author: Maximilian Anzinger
'''

import pygame

pygame.font.init()

class ImgButton( object ):
    
    '''
    this is a Button with two different images
    a text is optional
    '''
    
    def __init__( self, ui, screen, xpos, ypos, width, height, uactiveImgDir, activeImgDir,
                  text = None, txpos = 0, typos = 0, size = 15, uacolor = ( 255, 255, 255 ), acolor = ( 255, 255, 255 ), fontDir = None, command = None ):
        
        # containment
        self.__UI = ui
        self.__SCREEN = screen
        
        # positioning of the button
        self.__xpos = int( ( xpos / float( 100 ) ) * self.__UI.getSize()[ 0 ] )
        self.__ypos = int( ( ypos / float( 100 ) ) * self.__UI.getSize()[ 1 ] )
        self.__width = int( ( width / float( 100 ) ) * self.__UI.getSize()[ 0 ] )
        self.__height = int( ( height / float( 100 ) ) * self.__UI.getSize()[ 1 ] )
        
        # text attributes
        self.__text = text
        self.__txpos = self.__xpos + int( ( txpos / float( 100 ) ) * self.__xpos )
        self.__typos = self.__ypos + int( ( typos / float( 100 ) ) * self.__ypos )
        self.__textSize = size
        self.__uatextColor = uacolor
        self.__atextColor = acolor
        self.__actualColor = uacolor
        self.__fontDir = fontDir
        
        # load and convert images
        self.__uactivImg = pygame.image.load( uactiveImgDir )
        if ( self.__uactivImg.get_alpha() == None ):
            self.__uactivImg = self.__uactivImg.convert()
        else:
            self.__uactivImg = self.__uactivImg.convert_alpha()
        self.__uactivImg = pygame.transform.scale( self.__uactivImg, ( self.__width, self.__height ) )
        
        self.__activImg = pygame.image.load( activeImgDir )
        if ( self.__activImg.get_alpha() == None ):
            self.__activImg = self.__activImg.convert()
        else:
            self.__activImg = self.__activImg.convert_alpha()
        self.__activImg = pygame.transform.scale( self.__activImg, ( self.__width, self.__height ) )
        
        self.__buttonImg = self.__uactivImg
        
        # collision parameters
        self.__active = False
        self.__clicked = False
        self.__clickedTime = 0
        self.__COOLDOWN = 0
        
        # command
        if ( command == None ):
            self.__command = lambda: self.noCommand()
        else:
            self.__command = command
        
        self.createButton()
            
    #################### getters ####################
    
    def getSize( self ):
        
        return self.__buttonImg.get_rect().size
    
    def getClicked( self ):
        
        return self.__clicked
        
    #################### setters ####################
        
    def setActive( self, value ):
        
        self.__active = value
    
    def setImg( self ):
        
        if ( self.__active == True ):
            img = self.__activImg
        else:
            img = self.__uactivImg
        return img
    
    def setColor( self ):
        
        if ( self.__active == True ):
            color = self.__atextColor
        else:
            color = self.__uatextColor
        return color
    
    def setText( self, value ):
        
        self.__text = value
    
    #################################################
    
    def detectCollision( self, x, y ):
        
        if ( self.__xpos < x ) and ( x < self.__xpos + self.getSize()[ 0 ] ):
            if ( self.__ypos < y ) and ( y < self.__ypos + self.getSize()[ 1 ] ):
                try:
                    if ( self.__buttonImg.get_at( ( x - self.__xpos, y - self.__ypos ) )[ 3 ] == 255 ): # full visible pixel
                        collision = True
                    else:
                        collision = False
                except:
                    collision = False
            else:
                collision = False
        else:
            collision = False
        
        return collision
    
    def addText( self ):
        
        font = pygame.font.Font( self.__fontDir, self.__textSize )
        text = font.render( self.__text, True, self.__actualColor )
        self.__SCREEN.blit( text, ( self.__txpos, self.__typos ) )
    
    def createButton( self ):

        self.__SCREEN.blit( self.__buttonImg, ( self.__xpos, self.__ypos ) )
        
        if ( self.__text != None ):
            self.addText()
            
    def Command( self ):
        
        self.__command()
        self.__active = False
        self.__clicked = False

    def noCommand( self ):
        
        print 'button without command'
        self.__active = False
        self.__clicked = False
    
    def updateButton( self, x, y, clicked, time ):
        
        self.__active = self.detectCollision( x, y )
        if ( self.__active == True ):
            self.__clicked = clicked
        if ( self.__clicked == True ):
            if ( self.__clickedTime + self.__COOLDOWN <= time ):
                self.Command()
                self.__clickedTime = time
        else:
            self.__buttonImg = self.setImg()
            self.__actualColor = self.setColor()
            self.createButton()






class Token( object ):
    
    '''
    kind of a button representing the characters of the players
    '''
    
    def __init__( self, ui, screen, pos, xpos, ypos, width, height, imgs ):
        
        # containment
        self.__UI = ui
        self.__SCREEN = screen
        
        # positioning of the token
        self.__pos = pos
        self.__xpos = int( ( xpos / float( 100 ) ) * self.__UI.getSize()[ 0 ] )
        self.__ypos = int( ( ypos / float( 100 ) ) * self.__UI.getSize()[ 1 ] )
        self.__width = int( ( width / float( 100 ) ) * self.__UI.getSize()[ 0 ] )
        self.__height = int( ( height / float( 100 ) ) * self.__UI.getSize()[ 1 ] )
        
        # images
        self.__IMGS = imgs
        
        for img in self.__IMGS:
            self.__IMGS[ img ] = pygame.transform.scale( self.__IMGS[ img ], ( self.__width, self.__height ) )
        
        self.__actualImg = self.__IMGS[ 'black' ]
        
        # collision parameters
        self.__status = 'black'
        self.__hit = False
        self.__active = False
        self.__clicked = False
        self.__clickedTime = 0
        self.__COOLDOWN = 0
        
        self.createButton()
        
    #################### getters ####################
    
    def getStatus( self ):
        
        status = self.__UI.getField()[ self.__pos[ 0 ] ][ self.__pos[ 1 ] ]
        
        if ( status == 0 ):
            status = 'black'
        elif ( status == 1 ):
            status = 'blue'
        elif ( status == 2 ):
            status = 'red'
            
        return status
    
    def getHit( self ):
        
        if ( self.__pos in self.__UI.getImpactOpportunities()[ 0 ] ) or ( self.__pos in self.__UI.getImpactOpportunities()[ 1 ] ):
            self.__hit = True
        else:
            self.__hit = False
            
    def getActive( self ):
        
        return self.__active
    
    def getSize( self ):
        
        return self.__actualImg.get_rect().size
    
    def getClicked( self ):
        
        return self.__clicked
    
    #################### setters ####################
    #################################################    
    def setStatus( self, value ):
        
        self.__status = value
        

            
    def setActive( self, value ):
        
        self.__active = value
            
    def setImg( self ):
        
        self.setStatus( self.getStatus() )
        
        if ( self.__active == True ):
            self.__status += 'h'
            
        img = self.__IMGS[ self.__status ]
        
        return img
    
    #################################################
    
    def detectCollision( self, x, y ):
        
        if ( self.__xpos < x ) and ( x < self.__xpos + self.getSize()[ 0 ] ):
            if ( self.__ypos < y ) and ( y < self.__ypos + self.getSize()[ 1 ] ):
                try:
                    if ( self.__actualImg.get_at( ( x - self.__xpos, y - self.__ypos ) )[ 3 ] == 255 ): # full visible pixel
                        collision = True
                    else:
                        collision = False
                except:
                    collision = False
            else:
                collision = False
        else:
            collision = False
        
        return collision
    
    def createButton( self ):

        self.__SCREEN.blit( self.__actualImg, ( self.__xpos, self.__ypos ) )
        
        if ( self.__hit == True ):
            self.__SCREEN.blit( self.__IMGS[ 'hit' ], ( self.__xpos, self.__ypos ) )
            
    def Command( self ):
        
        self.__active = False
        self.__clicked = False
        self.__UI.MANAGER.checkToken( self.__pos )
    
    def updateButton( self, x, y, clicked, time ):
        
        realActive = self.detectCollision( x, y )
        unrealActive = False
        if ( realActive == True ):
            self.__active = True
            if ( self.__pos in self.__UI.getImpactOpportunities() ):
                for group in self.__UI.getImpactOpportunities():
                    for pos in group:
                        self.__UI.tokens[ str( pos[ 1 ] + 1 ) + str( pos[ 0 ] + 1 ) ].setActive( False )
            
        else:
            if ( self.__pos in self.__UI.getImpactOpportunities()[ 0 ] ):
                for pos in self.__UI.getImpactOpportunities()[ 0 ]:
                    if ( self.__UI.tokens[ str( pos[ 1 ] + 1 ) + str( pos[ 0 ] + 1 ) ].getActive() == True ):
                        self.__active = True
                        unrealActive = True
            elif ( self.__pos in self.__UI.getImpactOpportunities()[ 1 ] ):
                for pos in self.__UI.getImpactOpportunities()[ 1 ]:
                    if ( self.__UI.tokens[ str( pos[ 1 ] + 1 ) + str( pos[ 0 ] + 1 ) ].getActive() == True ):
                        self.__active = True
                        unrealActive = True
            else:
                self.__active = False
                
        if ( self.__active == True ):
            self.__clicked = clicked
        if ( self.__clicked == True ):
            if ( self.__clickedTime + self.__COOLDOWN <= time ):
                self.Command()
                self.__clickedTime = time
        else:
            self.__actualImg = self.setImg()
            self.getHit()
            self.createButton()
            if ( unrealActive == True ):
                self.__active = False
                
                
                
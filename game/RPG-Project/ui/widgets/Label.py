
'''
created on 09.06.2015

@author: Maximilian Anzinger
'''

import pygame
import os

pygame.font.init()

class ImgLabel( object ):
    
    '''
    class to view a image with a optional text infront of it
    '''
    
    def __init__( self, ui, screen, xpos, ypos, width, height, bgImgDir,
                  text = None, txpos = 0, typos = 0, size = 15, color = ( 255, 255, 255 ), fontDir = None ):
        
        # containment
        self.__UI = ui
        self.__SCREEN = screen
        
        # position of the picture
        self.__xpos = int( ( xpos / float( 100 ) ) * self.__UI.getSize()[ 0 ] )
        self.__ypos = int( ( ypos / float( 100 ) ) * self.__UI.getSize()[ 1 ] )
        self.__width = int( ( width / float( 100 ) ) * self.__UI.getSize()[ 0 ] )
        self.__height = int( ( height / float( 100 ) ) * self.__UI.getSize()[ 1 ] )
        
        self.__bgImgDir = bgImgDir
        
        # position of the text in the picture
        self.__text = text
        self.__txpos = self.__xpos + int( ( txpos / float( 100 ) ) * self.__xpos )
        self.__typos = self.__ypos + int( ( typos / float( 100 ) ) * self.__ypos )
        self.__textSize = size
        self.__color = color
        self.__fontDir = fontDir
        
        self.createLabel()
    
    def changeText( self, text ):
        
        self.__text = text
        self.updateLabel()
        
    def changeBgImg( self, bgImgDir ):
        
        self.__bgImgDir = bgImgDir
        self.updateLabel()
        
    def setText( self ):
        
        # load font
        font = pygame.font.Font( self.__fontDir, self.__textSize )
        
        # render text
        if ( type( self.__text ) == str ):
            text = font.render( self.__text, True, self.__color )
            self.__SCREEN.blit( text, ( self.__txpos, self.__typos ) )
        else: # used to enable wordwrap
            accumulatedHeight = self.__typos
            for line in self.__text:
                textline = font.render( line, True, self.__color )
                self.__SCREEN.blit( textline,  ( self.__txpos, accumulatedHeight ) )
                accumulatedHeight += font.size( line )[ 1 ]
        
    def addBgImg( self ):
        
        # load image
        self.__bgImg = pygame.image.load( self.__bgImgDir )
        
        # convert image
        if ( self.__bgImg.get_alpha() == None ):
            self.__bgImg = self.__bgImg.convert()
        else:
            self.__bgImg = self.__bgImg.convert_alpha()
            
        # scale image
        self.__bgImg = pygame.transform.scale( self.__bgImg, ( self.__width, self.__height ) )
        
        # render image
        self.__SCREEN.blit( self.__bgImg, ( self.__xpos, self.__ypos ) )
        
    def createLabel( self ):
        
        self.addBgImg()
        if ( self.__text != None ):
            self.setText()
            
    def updateLabel( self ):
        
        self.createLabel()
        




class GameLog( ImgLabel ):
    
    '''
    kind of a label to show the last hits
    '''
    
    def __init__( self, manager, ui, screen, xpos, ypos, width, height, bgImgDir,
                  text = None, txpos = 0, typos = 0, size = 15, color = ( 255, 255, 255 ), fontDir = None ):
        
        ImgLabel.__init__( self, ui, screen, xpos, ypos, width, height, bgImgDir, text, txpos, typos, size, color, fontDir )
        
        # containment
        self.__MANAGER = manager
        
        # log parameters
        self.__moves = []
        self.__logText = []
        self.__viewmoves = 5
        
    def getLastProgress( self ):
        
        return self.__moves
    
    def clearProgress( self ):
        
        self.__moves = []
        self.__logText = []
        self.changeText( '' )
        
    def updateMoves( self, player, move ):
        
        if ( len( self.__moves ) > ( self.__viewmoves - 1 ) ):
            del self.__moves[ 0 ], self.__logText[ 0 : 3 ]
        
        self.__moves.append( move )
        self.__logText += list( self.convertMove( player, move ) )
        
        self.changeText( self.__logText )
        
    def convertMove( self, player, move ):
        
        startpos = chr( move[ 0 ][ 1 ] + 65 ) + str( move[ 0 ][ 0 ] + 1 )
        aimpos = chr( move[ 1 ][ 1 ] + 65 ) + str( move[ 1 ][ 0 ] + 1 )
        hits = ''
        for token in move[ 2 ]:
            hits += chr( token[ 1 ] + 65) + str( token[ 0 ] + 1 ) + ', '
            
        movetext = self.__MANAGER.PLAYERS[ 'player' + str( player ) ].getName() + ' from ' + startpos + ' to ' + aimpos
        hittext = 'hits: ' + hits[ 0 : -2 ]
        
        return movetext, hittext, ''
        
        
        
        
        
class StatusDisplay( ImgLabel ):
    
    '''
    kind of a label to show error messages
    '''
    
    def __init__( self, manager, ui, screen, xpos, ypos, width, height, bgImgDir,
                  text = None, txpos = 0, typos = 0, size = 15, color = ( 255, 255, 255 ), fontDir = None ):
        
        ImgLabel.__init__( self, ui, screen, xpos, ypos, width, height, bgImgDir, text, txpos, typos, size, color, fontDir )
        
        # containment
        self.__MANAGER = manager
        
        # display parameters
        self.__EVENTS = {
                         'ard' : [ 'the move has', 'already been done' ],
                         'ns' : 'this token is neutral',
                         'nyt' : 'not your turn',
                         'rw' : 'red win',
                         'bw' : 'blue win',
                         'dp' : 'discontinued process'
                         }
        
        self.__COOLDOWN = 2000
        self.__currentEvent = ''
        self.__eventTime = -self.__COOLDOWN
        
    def updateStatText( self, time, event = None ):

        if ( event != None ):
            self.__currentEvent = event
            self.__eventTime = time
            text = self.__EVENTS[ self.__currentEvent ]
        else:
            if ( time < self.__eventTime + self.__COOLDOWN ):
                text = self.__EVENTS[ self.__currentEvent ]
            else:
                player = ( self.__MANAGER.getMoves() % 2 ) + 1
                text = self.__MANAGER.PLAYERS[ 'player' + str( player ) ].getName() + "'s turn"
        
        self.changeText( text )
        
        
        
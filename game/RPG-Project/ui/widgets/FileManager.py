
'''
created on 14.06.2015

@author: Maximilian Anzinger
'''

import os
import Tkinter as Tk
from tkFileDialog import askopenfilename
from tkFileDialog import asksaveasfilename


def findExistingFiles( mainDir, fDir ):
    
    # returns list of existing files in search directory
    return os.listdir( os.path.join( mainDir, fDir ) )

def createPaths( fileNames, existingFiles, mainDir, fDir, endCut ):
    
    # returns dictionary of paths to the existing files in the search directory
    filePaths = {}
    
    for file in fileNames:
        if ( file in existingFiles ):
            path = os.path.join( mainDir, fDir, file )
            filePaths[ file[ 0 : endCut ] ] = path
        else:
            print 'the file does not exist'
    
    return filePaths

def importTxtFile( dir ):
    
    file = open( dir, 'r' )
    content = file.readlines()
    file.close()
    
    for pos, line in enumerate( content ):
        content[ pos ] = line[ : -1 ]
    
    return content





class SaveSystem( object ):
    
    '''
    class to enable to save or load the game state
    '''
    
    def __init__( self, manager ):
        
        self.__MANAGER = manager
        self.__MAINDIR = self.__MANAGER.getMainDir()
        
    def loadGame( self ):
        
        # UI / dialog; generate file path
        self.auxiliaryWindow()
        path = self.loadGameDialog()
        self.window.destroy()
        
        # calculate parameters
        file = importTxtFile( path )
        
        # create game field
        field = [ list( a ) for a in file[ 0 : 5 ] ]
        for ypos, row in enumerate( field ):
            for xpos, token in enumerate( row ):
                field[ ypos ][ xpos ] = int( token )
        
        # moves already done
        moves = int( file[ 5 ] )
        
        # create history for the game log
        history = [ list( a ) for a in file[ 6 : ] ]
        for i, line in enumerate( history ):
            move = [[], []]
            hits = []
            for pos, coordinate in enumerate( line ):
                coordinate = int( coordinate )
                if ( pos == 0 ) or ( pos == 1 ):
                    move[ 0 ].append( coordinate )
                elif ( pos == 2 ) or ( pos == 3 ):
                    move[ 1 ].append( coordinate )
                else:
                    if ( pos%2 == 0 ):
                        hits.append( [ coordinate ] )
                    else:
                        hits[ -1 ].append( coordinate )
            hits = [ tuple( a ) for a in hits ]
            history[ i ] = [ tuple( move[ 0 ] ), tuple( move[ 1 ] ), tuple( hits ) ]
        
        return field, moves, history
    
    def saveGame( self, field, moves, lastprogress ):
        
        # UI / dialog; generate file path
        self.auxiliaryWindow()
        path = self.saveGameDialog()
        self.window.destroy()
        
        # generate file value
        val = []
        
        for row in field: # field content
            row = [ str( i ) for i in row ]
            val.append( ''.join( row ) + '\n' )
        
        val.append( str( moves ) + '\n' ) # moves already done
        
        for move in lastprogress: # history
            movestr = str( move[ 0 ][ 0 ] ) + str( move[ 0 ][ 1 ] ) + str( move[ 1 ][ 0 ] ) + str( move[ 1 ][ 1 ] )
            for hit in move[ 2 ]:
                movestr += str( hit[ 0 ] ) + str( hit[ 1 ] )
            movestr += '\n'
            val.append( movestr )
        
        # save value
        file = open( path, 'w' )
        file.write( ''.join( val ) )
        file.close()
    
    ############################# dialogs #############################
    
    def loadGameDialog( self ):
        
        return askopenfilename( title = 'Choose a game',
                                initialdir = self.__MAINDIR + '/savedgames' )
    
    def saveGameDialog( self ):
        
        return asksaveasfilename( title = 'Select savedirectory',
                                  initialdir = self.__MAINDIR + '/savedgames',
                                  initialfile = 'rename.txt' )
        
    def auxiliaryWindow( self ):
        
        self.window = Tk.Tk()
        self.window.geometry( '0x0' )
        
        
        
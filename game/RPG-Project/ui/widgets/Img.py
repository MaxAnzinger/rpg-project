
'''
created on 04.06.2015

@author: Maximilian Anzinger
'''

import pygame

class Img( object ):
    
    def __init__( self, widget, screen, imgDir, xpos = 0, ypos = 0, width = 100, height = 100 ):
        
        # containment
        self.__widget = widget
        self.__screen = screen
        self.__imgDir = imgDir
        
        # positioning
        try:
            self.__xpos = int( ( xpos / float( 100 ) ) * self.__widget.getSize()[ 0 ] + self.__widget.getPos()[ 0 ] )
            self.__ypos = int( ( ypos / float( 100 ) ) * self.__widget.getSize()[ 1 ] + self.__widget.getPos()[ 1 ] )
        except:
            self.__xpos = int( ( xpos / float( 100 ) ) * self.__widget.getSize()[ 0 ] )
            self.__ypos = int( ( ypos / float( 100 ) ) * self.__widget.getSize()[ 1 ] )
        
        # size
        self.__width = int( ( width / float( 100 ) ) * self.__widget.getSize()[ 0 ] )
        self.__height = int( ( height / float( 100 ) ) * self.__widget.getSize()[ 1 ] )
        
        # load image
        self.__iMg = pygame.image.load( self.__imgDir )
        if ( self.__iMg.get_alpha() == None ):
            self.__iMg = self.__iMg.convert()
        else:
            self.__iMg = self.__iMg.convert_alpha()
        self.__iMg = pygame.transform.scale( self.__iMg, ( self.__width, self.__height ) )
        
        self.update()
        
    def update( self ):
        
        self.__screen.blit( self.__iMg, ( self.__xpos, self.__ypos ) )
        
        
        

'''
created on 29.01.2016

@author: Maximilian Anzinger
'''

import pygame
import os
import sys

pygame.init()
pygame.font.init()

class Misc( object ):
    
    def getSize( self ):
        return self.getWidth(), self.getHeight()
    
    def getWidth( self ):
        return self.__width
    
    def getHeight( self ):
        return self.__height
    
class BaseWidget( object ):
    
    def __init__( self ):
        pass

class Window( Misc ):
    
    def __init__( self, windowpos = ( 100, 100 ) ):
        
        self.setWindowPos( windowpos )
        
        self.__DIR = os.path.dirname( os.path.abspath( __file__ ) )
        
        self.__RESOLUTION = (800,800)
        self.__ICON = None
        self.__title = 'PygameGUI window'
        
        self.__imgs = {}
        self.__labels = {}
        self.__buttons = {}
        
        self.__vars = {
                       'tick' : 200
                       }
        
    def setWindowPos( self, pos ):
        os.environ[ 'SDL_VIDEO_WINDOW_POS' ] = "%d,%d" % ( pos )
    
    def setTitle( self, value ):
        self.__title = value
    
    def setMouseVisibility( self, value ):
        pygame.mouse.set_visible( value )
        
    def setKeyRepeat( self, delay, interval ):
        pygame.key.set_repeat( delay, interval )
        
    def setTick( self, value ):
        self.__vars[ 'tick' ] = value
        
    def setIcon( self, imgDir ):
        self.__ICON = imgDir
        
    def getTicks( self ):
        return self.__clock
    
    
    def mainloop( self ):
        
        self.__window = pygame.display.set_mode( self.__RESOLUTION, pygame.DOUBLEBUF|pygame.HWSURFACE )
        
        if ( self.__title != None ):
            pygame.display.set_caption( self.__title )
        if ( self.__ICON != None ):
            self.__ICON = pygame.image.load( self.__ICON )
            if ( self.__ICON.get_alpha() == None ):
                self.__ICON = self.__ICON.convert()
            else:
                self.__ICON = self.__ICON.convert_alpha()
            pygame.display.set_icon( self.__ICON )
        pygame.mouse.set_visible( True )
        pygame.key.set_repeat( 1, 30 )
        
        self.__clock = pygame.time.Clock()
        
        self.__runtime = True
        while self.__runtime:
            self.__clock.tick( self.__vars[ 'tick' ] )
            self._processInput( pygame.event.get() )
            pygame.display.flip()
            
    def _processInput( self, events ):
        
        for event in events:
            
            if ( event.type == pygame.QUIT ):
                sys.exit()
                
            elif ( event.type == pygame.MOUSEBUTTONDOWN ):
                x, y = event.pos
                self._updateContent()
                self._updateButtons( x, y, True )
                
            elif ( event.type == pygame.MOUSEMOTION ):
                x, y = event.pos
                self._updateContent()
                self._updateButtons( x, y, False )
                
            elif ( event.type == pygame.KEYDOWN ):
                if ( event.key == pygame.K_ESCAPE ):
                    pygame.event.post( pygame.event.Event( pygame.QUIT ) )
                    
    def _updateContent( self ):
        
        self._updateImgs()
        self._updateLabels()
    
    def _updateImgs( self ):
        
        for img in self.__imgs:
            self.__imgs[ img ].update()
    
    def _updateLabels( self ):
        
        for label in self.__labels:
            self.__labels[ label ].update()
    
    def _updateButtons( self, x, y, clicked ):
        
        for button in self.__buttons:
            self.__buttons[ button ].update( x, y, clicked, self.getTicks() )
    
    def addWidget( self ):
        pass




class PlacementManager( object ):
    
    def place( self, x, y ): # absolute position
        pass
    
    def pack( self ):
        pass
    
    def dyn( self ): # position in percentage
        pass

class Container( Misc, PlacementManager ):
    
    def __init__( self ):
        pass
    
    def addWidget( self ):
        pass

class Img( Misc, PlacementManager ):
    
    def __init__( self ):
        pass
    
    def update(self):
        pass

class Label( Misc, PlacementManager ):
    
    def __init__( self ):
        self.__IMGLABEL = False
        
    def update(self):
        pass

class ImgLabel( Label, Misc ):
    
    def __init__( self ):
        self.__IMGLABEL = True

class Button( Misc, PlacementManager ):
    
    def __init__( self ):
        self.__IMGBUTTON = False
    
    def update(self):
        pass

class ImgButton( Button, Misc ):
    
    def __init__( self ):
        self.__IMGBUTTON = True



###################################################################
# Test:

def _test():
    
    master = Window()
    mainPath = os.path.dirname( os.path.abspath( __file__ ) )
    master.setIcon( mainPath + '/imgs/icon.png')
    master.mainloop()
if __name__ == '__main__':
    _test()









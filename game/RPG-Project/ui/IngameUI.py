
'''
created on 14.06.2015

@author: Maximilian Anzinger
'''

import sys, os
import pygame

os.environ[ 'SDL_VIDEO_WINDOW_POS' ] = "%d,%d" % ( 10, 30 )
pygame.init()

class IngameUI( object ):
    
    '''
    class which includes the whole GUI
    '''
    
    def __init__( self, manager ):
        
        self.__MANAGER = manager
        self.__RESOLUTION = ( pygame.display.Info().current_w - 100, pygame.display.Info().current_h - 100 )
        self.__FDIR = 'UI/img'
        self.__EXISTINGFILES = []
        self.__FILEPATHS = {}
        self.__TEXT = {
                       'window-title' : 'Fanorona',
                       'newGameButton' : 'start new game',
                       'saveGameButton' : 'save game',
                       'loadGameButton' : 'load game',
                       'player1Button' : 'blue: User',
                       'player2Button' : 'red: User'
                       }
        self.__buttons = {}
        self.labels = {}
        
        # color tuples
        self.__WHITE = ( 255, 255, 255 )
        self.__RED = ( 255, 0, 0 )
        self.__BLACK = ( 0, 0, 0 )
        
        self.importWidgets()
        self.createFilePaths()
        
    def importWidgets( self ):
        
        sys.path.insert( 0, self.__MANAGER.getMainDir() + '/UI/Widgets' )
        
        import FileManager as FileManager
        import Button as Button
        import Label as Label
        import GameBoard as GameBoard
        global FileManager
        global Button
        global Label
        global GameBoard
    
    #################### getters ####################
    
    def getSize( self ):
        
        return self.__window.get_size()
    
    def getTicks( self ):
        
        return pygame.time.get_ticks()
    
    def getInput( self, events ):
        
        #process events
        for event in events:
            
            '''
            # under first if-clause
            # funktion has to be optimized; UI restarts...
            elif event.type == pygame.VIDEORESIZE:
                self.__resolution = event.size
                self.setupMainWindow()
            '''
            
            if ( event.type == pygame.QUIT ):
                sys.exit()
            
            elif ( event.type == pygame.MOUSEBUTTONDOWN ):
                x, y = event.pos
                self.updateContent()
                self.updateButtons( x, y, True )
            
            elif ( event.type == pygame.MOUSEMOTION ):
                x, y = event.pos
                self.updateContent()
                self.updateButtons( x, y, False )

            elif ( event.type == pygame.KEYDOWN ):
                if ( event.key == pygame.K_ESCAPE ):
                    pygame.event.post( pygame.event.Event( pygame.QUIT ) )
        
        # investigate field
        tokenp1, tokenp2 = 0, 0
        for row in self.gameBoard.getField():
            tokenp1 += row.count( 1 )
            tokenp2 += row.count( 2 )
        
        mintoken = min( tokenp1, tokenp2 )
        if ( mintoken != 0 ):
            self.__MANAGER.checkAI()
    
    #################################################
    
    def createFilePaths( self ):
        
        self.__EXISTINGFILES = FileManager.findExistingFiles( self.__MANAGER.getMainDir(), self.__FDIR )
        self.__FILEPATHS = FileManager.createPaths( self.__EXISTINGFILES, self.__EXISTINGFILES, self.__MANAGER.getMainDir(), self.__FDIR, -4 )
        
        
        
    def setupMainWindow( self ):
        
        self.__window = pygame.display.set_mode( self.__RESOLUTION, pygame.DOUBLEBUF|pygame.HWSURFACE ) #pygame.RESIZABLE|pygame.DOUBLEBUF|pygame.HWSURFACE) # optional
        
        pygame.display.set_caption( self.__TEXT[ 'window-title' ] )
        pygame.mouse.set_visible( True )
        pygame.key.set_repeat( 1, 30 )
        
        self.clock = pygame.time.Clock()
        
        self.addContent()
        
        self.runtime = True
        
        while self.runtime:
            self.clock.tick( 200 )
            self.getInput( pygame.event.get() )
            pygame.display.flip()

    def addContent( self ):
        
        self.__SCREEN = pygame.display.get_surface()
        
        ############################## buttons ##############################
        
        self.__buttons[ 'newGameButton' ] = Button.ImgButton( self,
                                                              self.__SCREEN,
                                                              83, 10,
                                                              15, 2,
                                                              self.__FILEPATHS[ 'button1' ],
                                                              self.__FILEPATHS[ 'button2' ],
                                                              text = self.__TEXT[ 'newGameButton' ],
                                                              size = 15,
                                                              uacolor = self.__RED,
                                                              acolor = self.__WHITE,
                                                              fontDir = self.__MANAGER.getFont( 'pixel emulator' ),
                                                              command = lambda: self.__MANAGER.startNewGame() )
        
        self.__buttons[ 'saveGame' ] = Button.ImgButton( self,
                                                         self.__SCREEN,
                                                         83, 15,
                                                         15, 2,
                                                         self.__FILEPATHS[ 'button1' ],
                                                         self.__FILEPATHS[ 'button2' ],
                                                         text = self.__TEXT[ 'saveGameButton' ],
                                                         size = 15,
                                                         uacolor = self.__RED,
                                                         acolor = self.__WHITE,
                                                         fontDir = self.__MANAGER.getFont( 'pixel emulator' ),
                                                         command = lambda: self.__MANAGER.saveGame() )
        
        self.__buttons[ 'loadGame' ] = Button.ImgButton( self,
                                                         self.__SCREEN,
                                                         83, 20,
                                                         15, 2,
                                                         self.__FILEPATHS[ 'button1' ],
                                                         self.__FILEPATHS[ 'button2' ],
                                                         text = self.__TEXT[ 'loadGameButton' ],
                                                         size = 15,
                                                         uacolor = self.__RED,
                                                         acolor = self.__WHITE,
                                                         fontDir = self.__MANAGER.getFont( 'pixel emulator' ),
                                                         command = lambda: self.__MANAGER.loadGame() )
        
        self.__buttons[ 'player1' ] = Button.ImgButton( self,
                                                        self.__SCREEN,
                                                        83, 30,
                                                        15, 2,
                                                        self.__FILEPATHS[ 'button1' ],
                                                        self.__FILEPATHS[ 'button2' ],
                                                        text = self.__TEXT[ 'player1Button' ],
                                                        size = 15,
                                                        uacolor = self.__RED,
                                                        acolor = self.__WHITE,
                                                        fontDir = self.__MANAGER.getFont( 'pixel emulator' ),
                                                        command = lambda: self.__MANAGER.changePlayerManner( 1 ) )
        
        self.__buttons[ 'player2' ] = Button.ImgButton( self,
                                                        self.__SCREEN,
                                                        83, 35,
                                                        15, 2,
                                                        self.__FILEPATHS[ 'button1' ],
                                                        self.__FILEPATHS[ 'button2' ],
                                                        text = self.__TEXT[ 'player2Button' ],
                                                        size = 15,
                                                        uacolor = self.__RED,
                                                        acolor = self.__WHITE,
                                                        fontDir = self.__MANAGER.getFont( 'pixel emulator' ),
                                                        command = lambda: self.__MANAGER.changePlayerManner( 2 ) )
        
        ############################## labels ##############################
        
        self.labels[ 'log' ] = Label.GameLog( self.__MANAGER,
                                              self,
                                              self.__SCREEN,
                                              83, 45,
                                              15, 20,
                                              self.__FILEPATHS[ 'button1' ],
                                              text = '',
                                              size = 10,
                                              color = self.__RED,
                                              fontDir = self.__MANAGER.getFont( 'pixel emulator' ) )
        
        self.labels[ 'statdisp' ] = Label.StatusDisplay( self.__MANAGER,
                                                         self,
                                                         self.__SCREEN,
                                                         83, 70,
                                                         15, 15,
                                                         self.__FILEPATHS[ 'button1' ],
                                                         text = '',
                                                         size = 15,
                                                         color = self.__RED,
                                                         fontDir = self.__MANAGER.getFont( 'pixel emulator' ) )
        
        ############################## game board ##############################
        
        self.gameBoard = GameBoard.GameBoard( self.__MANAGER, self, self.__SCREEN, 10, 10, 70, 75, self.__FILEPATHS )
        
    def updateContent( self ):
        
        self.gameBoard.update()
        self.labels[ 'log' ].updateLabel()
        self.labels[ 'statdisp' ].updateStatText( self.getTicks() )
    
    def updateButtons( self, x, y, clicked ):
        
        for button in self.__buttons:
            self.__buttons[ button ].updateButton( x, y, clicked, self.getTicks() )
        for token in self.gameBoard.tokens:
            self.gameBoard.tokens[ token ].updateButton( x, y, clicked, self.getTicks() )
            
    def updateLog( self, player, move ):
        
        self.labels['log'].updateMoves( player, move )
            
    def changeButtonText( self, button, value ):
        
        self.__buttons[button].setText(value)
        
        
        

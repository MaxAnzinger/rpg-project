
'''
created on 29.01.2016

@author: Maximilian Anzinger
'''

import sys

class Manager( object ):
    
    '''
    class to coordinate the logical with the optical part of the programm
    '''
    
    def __init__( self, mainDir ):
        
        # main parameters
        self.__MAINDIR = mainDir
        self.__FONTS = { 'pixel emulator' : mainDir + '/Fonts/Pixel Emulator.fon' }
        self.PLAYERS = {}
        self.__moves = 0
        self.__pos = ()
        self.__move = [ (), (), () ]
        self.__possibleMoves = []
        self.__gameRuns = True
        

        self.importObjects()
        self.setupAI()
        self.addPlayers()
        self.setupUI()
    
    #################### getters ####################
    
    def getMainDir( self ):
        
        return self.__MAINDIR
    
    def getFont( self, key ):
        
        return self.__FONTS[ key ]
    
    def getMoves( self ):
        
        return self.__moves
    
    #################### setters ####################
    
    def setMovePermission( self ):
        
        self.__moves += 1
        
        if ( self.__moves % 2 == 0 ):
            self.PLAYERS[ 'player1' ].setMove( True )
            self.PLAYERS[ 'player2' ].setMove( False )
        else:
            self.PLAYERS[ 'player1' ].setMove( False )
            self.PLAYERS[ 'player2' ].setMove( True )
    
    #################################################

    def importObjects( self ):
        
        sys.path.insert( 0, self.getMainDir() + '/UI' )
        import IngameUI as IngameUI
        global IngameUI
        
        sys.path.insert( 0, self.getMainDir() + '/UI/Widgets' )
        import FileManager as FileManager
        global FileManager
        
        sys.path.insert( 0, self.getMainDir() + '/Logic' )
        import AI as AI
        import Player as Player
        global AI
        global Player
        
    def setupAI( self ):
        
        self.AI = AI.AI( self )
        
    def setupUI( self ):
        
        self.__saveSystem = FileManager.SaveSystem( self )
        self.inGameUI = IngameUI.IngameUI( self )
        self.inGameUI.setupMainWindow()
        
    def startNewGame( self ):
         
        self.__moves = 0
        
        # reset players
        self.addPlayers()
        self.inGameUI.changeButtonText( 'player1', self.PLAYERS[ 'player1' ].getName() + ': ' + self.PLAYERS[ 'player1' ].getManner() )
        self.inGameUI.changeButtonText( 'player2', self.PLAYERS[ 'player2' ].getName() + ': ' + self.PLAYERS[ 'player2' ].getManner() )
        
        fields = [ [ 2, 2, 2, 2, 2, 2, 2, 2, 2 ],
                   [ 2, 2, 2, 2, 2, 2, 2, 2, 2 ],
                   [ 2, 1, 2, 1, 0, 2, 1, 2, 1 ],
                   [ 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
                   [ 1, 1, 1, 1, 1, 1, 1, 1, 1 ] ]

        self.inGameUI.gameBoard.setField( fields )
        self.inGameUI.gameBoard.setImpactOpportunities( [ [], [] ] )
        
        self.inGameUI.labels[ 'log' ].clearProgress()
        
    def addPlayers( self ):
        
        self.PLAYERS[ 'player1' ] = Player.Player( 'blue', 'user', True )
        self.PLAYERS[ 'player2' ] = Player.Player( 'red', 'user', False )
                          
    def moveToken( self, pos ):

        if ( self.inGameUI.gameBoard.getImpactOpportunities() == [ [],[] ] ):
            # move Token
            owner = self.__moves % 2 + 1
            self.inGameUI.gameBoard.setFieldAttribute( pos[ 0 ], pos[ 1 ], owner ) # set aim position
            self.inGameUI.gameBoard.setFieldAttribute( self.__pos[ 0 ], self.__pos[ 1 ], 0 ) # clear start position
            
            # calculate possible hits
            impactOpportunities = self.AI.calcImpact( self.__moves % 2 + 1, self.__pos, pos )
            self.inGameUI.gameBoard.setImpactOpportunities( impactOpportunities )
        else:
            self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'ard' )
        
        if ( self.inGameUI.gameBoard.getImpactOpportunities() == [ [],[] ] ): # no possible hit; end round
            self.setMovePermission()
            
    def checkToken( self, pos ):
        
        #get owner
        owner = self.inGameUI.gameBoard.getField()[ pos[ 0 ] ][ pos[ 1 ] ]
        
        if ( owner == 0 ):
            if ( pos in self.__possibleMoves ):
                self.moveToken( pos )
                self.__possibleMoves = []
                self.__move[ 1 ] = pos
            else:
                self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'ns' )
                
        elif ( owner == 1 ):
            if ( self.PLAYERS[ 'player1' ].getMove() == True ):
                self.__pos = pos
                self.__possibleMoves = self.AI.findPossibleMoves( pos )
                self.__move[ 0 ] = pos
            else:
                if ( pos in self.inGameUI.gameBoard.getImpactOpportunities()[ 0 ] ) or ( pos in self.inGameUI.gameBoard.getImpactOpportunities()[ 1 ] ):
                    self.hitTokens( pos )
                else:
                    self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'nyt' )
                    
        elif ( owner == 2 ):
            if ( self.PLAYERS[ 'player2' ].getMove() == True ):
                self.__pos = pos
                self.__possibleMoves = self.AI.findPossibleMoves( pos )
                self.__move[ 0 ] = pos
            else:
                if ( pos in self.inGameUI.gameBoard.getImpactOpportunities()[ 0 ] ) or ( pos in self.inGameUI.gameBoard.getImpactOpportunities()[ 1 ] ):
                    self.hitTokens( pos )
                else:
                    self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'nyt' )
            
    def hitTokens( self, pos ):
        
        if ( pos in self.inGameUI.gameBoard.getImpactOpportunities()[ 0 ] ):
            impactGroup = 0
        else:
            impactGroup = 1
        
        for token in self.inGameUI.gameBoard.getImpactOpportunities()[ impactGroup ]: # clear hit positions
            self.inGameUI.gameBoard.setFieldAttribute( token[ 0 ], token[ 1 ], 0 )
            
        self.__move[ 2 ] = tuple( self.inGameUI.gameBoard.getImpactOpportunities()[ impactGroup ] )
        
        self.inGameUI.gameBoard.setImpactOpportunities( [ [], [] ] )
        
        self.checkGameState()
        
        if ( self.__gameRuns == True ):
            self.updateLog( self.__move )
            self.setMovePermission()
            self.__move = [ (), (), () ]
            
        self.__gameRuns = True
        
    def checkGameState( self ):
        
        # count tokens
        tokenp1, tokenp2 = 0, 0
        for row in self.inGameUI.gameBoard.getField():
            tokenp1 += row.count( 1 )
            tokenp2 += row.count( 2 )
            
        if ( tokenp1 == 0 ):
            self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'rw' )
            self.startNewGame()
            self.__gameRuns = False
        elif ( tokenp2 == 0 ):
            self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'bw' )
            self.startNewGame()
            self.__gameRuns = False

    def changePlayerManner( self, player ):
        
        if ( self.PLAYERS[ 'player' + str( player ) ].getManner() == 'ai' ):
            self.PLAYERS[ 'player' + str( player ) ].setManner( 'user' )
        else:
            self.PLAYERS[ 'player' + str( player ) ].setManner( 'ai' )
            
        self.inGameUI.changeButtonText( 'player' + str( player ), self.PLAYERS[ 'player' + str( player ) ].getName() + ': ' + self.PLAYERS[ 'player' + str( player ) ].getManner() )
        
    def checkAI( self ):
        
        if ( self.PLAYERS[ 'player1' ].getManner() == 'ai' ) and ( self.PLAYERS[ 'player1' ].getMove() == True ):
            self.AI.calculateAiMove( 1 )
        if ( self.PLAYERS[ 'player2' ].getManner() == 'ai' ) and ( self.PLAYERS[ 'player2' ].getMove() == True ):
            self.AI.calculateAiMove( 2 )
    
    def moveAI( self, player, move ):

        self.inGameUI.gameBoard.setFieldAttribute( move[ 0 ][ 0 ], move[ 0 ][ 1 ], 0 ) # clear start position
        self.inGameUI.gameBoard.setFieldAttribute( move[ 1 ][ 0 ], move[ 1 ][ 1 ], player ) # set aim position
        
        for pos in move[ 2 ]:
            self.inGameUI.gameBoard.setFieldAttribute( pos[ 0 ], pos[ 1 ], 0 ) # clear hit positions
            
        self.checkGameState()
        self.setMovePermission()
        self.updateLog( move )
        
    def updateLog( self, move ):
        
        player = ( self.__moves % 2 ) + 1
        self.inGameUI.updateLog( player, move )
    
    def saveGame( self ):
        
        if ( self.__moves != 0 ):
            try:
                self.__saveSystem.saveGame( self.inGameUI.gameBoard.getField(),
                                            self.__moves,
                                            self.inGameUI.labels[ 'log' ].getLastProgress() )
            except:
                self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'dp' )
        
    def loadGame( self ):
        
        try:
            field, self.__moves, history = self.__saveSystem.loadGame()
            self.inGameUI.gameBoard.setField( field )
            self.__moves -= 1
            self.setMovePermission()
            for i, move in enumerate( history ):
                player = ( self.__moves - ( len( history ) - i ) ) % 2 + 1
                self.inGameUI.updateLog( player, move )
        except:
            self.inGameUI.labels[ 'statdisp' ].updateStatText( self.inGameUI.getTicks(), event = 'dp' )
    
    
    

'''
created on 29.01.2016

@author: Maximilian Anzinger
'''

import os
import Manager as Manager

def main():
    
    # directory of the gamefolder
    MAINDIR = os.path.dirname( os.path.abspath( __file__ ) )
    app = Manager.Manager( MAINDIR )

if ( __name__ == '__main__' ):
    #SWAGGY
    main()